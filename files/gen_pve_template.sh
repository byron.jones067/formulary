#!/bin/bash

TEMPL_NAME="$1"
VM_ID="$2"

mem="1024"
disk_size="32G"
disk_stor="local-lvm"
net_bridge="vmbr0"

# create VM as the base for images
qm create "$VM_ID" \
    --name "$TEMPL_NAME" --ostype l26 \
    --memory "$mem" \
    --net0 virtio,bridge="$net_bridge"

# import cloud image to storage pool
qm importdisk "$VM_ID" "$IMG_NAME" "$disk_stor"
# attach disk to VM as a SCSI drive
qm set "$VM_ID" --scsihw virtio-scsi-pci \
    --scsi0 "$disk_stor:vm-$VM_ID-disk-0"
# resize VM disk
qm resize "$VM_ID" scsi0 "$disk_size"
# create cloud-init drive
qm set "$VM_ID" --ide2 "$disk_stor":cloudinit
# make vm disk bootable
qm set "$VM_ID" --boot c --bootdisk scsi0
# assign serial console; NOTE required for cloud-init images
qm set "$VM_ID" --serial0 socket --vga serial0
# set network config to dhcp
qm set "$VM_ID" --ipconfig0 ip=dhcp
# enable the guest agent; NOTE required for proxmox webui
qm set "$VM_ID" --agent enabled=1

# convert VM to template
qm template "$VM_ID"
