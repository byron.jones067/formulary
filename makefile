
.DEFAULT_GOAL := help

.PHONY: help

help: ## Print help message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
 		sort | \
		awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

VENV_CMD = . .venv/bin/activate

clean: ## Delete environment
	rm -rf .venv

init: ## Create environment
	test -d .venv || python3 -m venv .venv
	$(VENV_CMD); python3 -m pip install -Ur requirements.txt

lint: ## Run linting checks
	$(VENV_CMD); pre-commit run --all
